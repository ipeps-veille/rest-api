import pyjsonrpc


class RequestHandler(pyjsonrpc.HttpRequestHandler):

    TODOS = {}
    TODO_ID = 1

    def create_new_todo(self, title):
        task = {
            'title': title,
            'status': 'pending',
            }
        todo_id = self.TODO_ID
        self.TODO_ID += 1
        self.TODOS[todo_id] = task
        return {'id': todo_id}


if __name__ == '__main__':
    server = pyjsonrpc.ThreadingHttpServer(
        server_address=('localhost', 8000),
        RequestHandlerClass=RequestHandler)
    server.serve_forever()
