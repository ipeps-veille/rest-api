from flask import Flask, jsonify, request, abort

app = Flask(__name__)
TODOS = {}
TODO_ID = 1


@app.route('/todo')
def get_all_todos():
    return jsonify(TODOS)


@app.route('/todo/<int:task_id>')
def get_todo(task_id):
    if task_id not in TODOS:
        abort(404)
    return jsonify(TODOS[task_id])


@app.route('/todo', methods={'POST'})
def create_new_todo():
    global TODO_ID
    if not request.json or not request.json.get('title'):
        abort(400)
    task = {
        'title': request.json['title'],
        'status': 'pending',
    }
    todo_id = TODO_ID
    TODO_ID += 1
    TODOS[todo_id] = task
    return {'id': todo_id}


@app.route('/todo/<int:task_id>', methods={'PATCH'})
def update_todo(task_id):
    if task_id not in TODOS:
        abort(404)
    if ('status' not in request.json
            or request.json['status'] not in {'done', 'pending'}):
        abort(400)
    TODOS[task_id]['status'] = 'done'
    return ''


@app.route('/todo/<int:task_id>', methods={'DELETE'})
def delete_todo(task_id):
    if task_id not in TODOS:
        abort(404)
    del TODOS[task_id]
    return ''


if __name__ == '__main__':
    app.run(debug=True)
