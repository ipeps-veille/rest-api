# Création d'une API REST

## Problématique

Communiquer avec le monde extérieur et offrir ses données pour permettre aux
entreprises ou aux particuliers de les utiliser est de plus en plus courant.

Notre recherche consiste donc à trouver la façon la plus simple de faire ceci
en python

## Le scénario de test

On veut donc créer une petite API qui offrira les fonctionnalités d'une petite
TODO-list:

    - créer un TODO
    - récupérer un TODO
    - récupérer tous les TODOs
    - marquer un TODO comme terminé
    - supprimer un TODO

## Recherches

Mots clés: *python webservices*

### Flask

Sous windows: https://insomnia.rest/

1. curl -i -H "Content-Type: application/json" -X POST -d '{"title": "Acheter Baba is You"}' http://localhost:5000/todo
2. curl -i http://localhost:5000/todo/1
3. curl -i http://localhost:5000/todo
4. curl -i -H "Content-Type: application/json" -X PATCH -d '{"status": "done"}
   http://localhost:5000/todo/1
5. curl -i -X DELETE http://localhost:5000/todo/1

Aller plus loin avec flask-restful ?

### pyjsonrpc

python2.7: NO GO!

### json-rpc

python3, c'est déjà ça
S'intégre à flask

### spyne

RPC toolkit, semble complexe

